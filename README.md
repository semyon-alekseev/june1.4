# The task is to run multi-stage building:
- make nginx bin from source files
- copy this to the second container in order to minimize the container's size
- add necessary files to run nginx server
- run all steps in one Dockerfile
***

# The result
- we have working nginx server showing our page

