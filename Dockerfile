FROM debian:9

RUN apt update -y && apt install -y wget gcc make libpcre++ libpcre++-dev zlib1g zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.9.9.tar.gz && tar xvzf nginx-1.9.9.tar.gz && cd nginx-1.9.9 && ./configure && make && make install

CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
